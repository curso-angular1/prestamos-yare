export class Empleado {
    constructor(
        public id_empleado: number,
        public nombre_empleado: string,
        public apellidos_empleado: string,
        public puesto_empleado: string,
        public telefono_empleado: string
    ) { }
}