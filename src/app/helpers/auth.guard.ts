import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { AuthService } from '../components/auth/auth.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(
    private AuthService: AuthService,
    private router: Router
  ) { }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): boolean {
    const estaAutenticado = this.AuthService.estaAutenticado();
    
    if (estaAutenticado) {
      return true;
    }

    this.AuthService.logout();
    this.router.navigateByUrl("auth/login");

    return false;;
  }

}
