import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';
import { Message, MessageService } from 'primeng/api';
import { OverlayService } from '../../overlay/overlay.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  providers: [MessageService]
})
export class LoginComponent implements OnInit {
  public loginform: FormGroup;
  public msgs1: Message[];

  constructor
    (
      private formBuilder: FormBuilder,
      private authService: AuthService,
      private messageService: MessageService,
      private router: Router,
      private overlayService: OverlayService
    ) {
    this.loginform = this.formBuilder.group({
      id_empleado: ['', Validators.required],
      contrasenia: ['', Validators.required]
    })
  }

  ngOnInit(): void {
  }

  get id_empleado() { return this.loginform.get('id_empleado') }
  get contrasenia() { return this.loginform.get('contrasenia') }

  addMensaje(msj: string) {
    this.msgs1 = [
      { severity: 'error', summary: 'Error', detail: msj },
    ];
  }


  login() {
    if (this.loginform.valid) {
      this.overlayService.iniciarLoading();
      this.authService.login(this.loginform.value)
        .subscribe(
          (res: any) => {
            this.authService.getUser().subscribe(res => {
              // console.log(res);
              this.router.navigate(['cajera']);
            }, error => {

              this.authService.logout();
              this.router.navigate(['/auth/login']);
            });


          }, (error) => {
            if (error.status == 401) {
              this.addMensaje(error.error.mensaje);
              return;
            }
          }
        ).add(() => {
          this.overlayService.detenerLoading();
        })
    }
  }

}
