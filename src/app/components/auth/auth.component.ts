import { Component, OnInit } from '@angular/core';
import { PrimeNGConfig } from 'primeng/api';
import { AuthService } from './auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
})
export class AuthComponent implements OnInit {

  constructor(
    private primengConfig: PrimeNGConfig,
    private authService: AuthService,
    private router:Router
  ) { }

  ngOnInit(): void {
    this.primengConfig.ripple = true;
    if (this.authService.estaAutenticado()) {
      //segun el rol
      this.router.navigate(['/cajera']);
    }
  }

}
