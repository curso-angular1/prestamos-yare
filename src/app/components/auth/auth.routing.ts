import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

//componentes

import { LoginComponent } from './login/login.component';
import { AuthComponent } from './auth.component';

const lazyroutes: Routes = [
  {
    path: '',
    component: AuthComponent,
    children: [
      { path: '', redirectTo: 'login', pathMatch: 'full' },
      {
        path: 'login',
        component: LoginComponent
      }]
  }

];

@NgModule({
  declarations: [],
  imports: [RouterModule.forChild(lazyroutes)],
  exports: [RouterModule]
})
export class AuthRouting { }