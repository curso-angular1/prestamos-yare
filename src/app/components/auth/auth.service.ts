import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from "rxjs/operators";
import { environment } from '@environments/environment.prod';
import { EmpleadoLogin } from '@app/models/login.interface';
import { Empleado } from '../../models/empleado.class';
import { BehaviorSubject, Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class AuthService {


  private token: string;
  private empleadoSubject: BehaviorSubject<Empleado>;
  public empleado$: Observable<Empleado>;

  constructor(private http: HttpClient) {
    this.getToken();
    this.empleadoSubject = new BehaviorSubject<Empleado>( JSON.parse(localStorage.getItem('user') || null));
    this.empleado$ = this.empleadoSubject.asObservable();

  }


  public get currentEmpleado(): Empleado {
    return this.empleadoSubject.value;
  }

  public getToken(): String {
    this.token = localStorage.getItem('token') || null;
    return this.token;
  }

  private guardarToken(resp: any) {
    this.token = resp.token;
    localStorage.setItem('token', resp.access_token);
    let hoy = new Date();
    hoy.setSeconds(resp.expires_in);
    localStorage.setItem('expira', hoy.getTime().toString());
  }

  login(empleado: EmpleadoLogin) {
    return this.http.post<any>(`${environment.url_api}/api/auth/login`, empleado)
      .pipe(map(resp => {
        this.guardarToken(resp);
        return resp;
      }));
  }
  logout() {
    localStorage.removeItem('token');
    localStorage.removeItem('expira');
    localStorage.removeItem('user');
    this.empleadoSubject.next(null);
  }

  estaAutenticado(): boolean {
    if (this.getToken() != null) {

      if (this.getToken().length < 2) {
        return false;
      }

      const expira = Number(localStorage.getItem('expira'));
      const expiraDate = new Date();
      expiraDate.setTime(expira);

      if (expiraDate > new Date()) {
        return true;
      } else {
        return false;
      }
    }
    return false;
  }



  getUser() {
    return this.http.get<Empleado>(`${environment.url_api}/api/auth/user`).pipe(map(resp => {

      localStorage.setItem('user', JSON.stringify(resp));
      this.empleadoSubject.next(resp);
      return resp;
    }));
  }




}
