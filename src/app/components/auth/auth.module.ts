//angular modules
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthRouting } from './auth.routing';
import { ReactiveFormsModule } from '@angular/forms';
import { FormsModule } from '@angular/forms';

//modulos
import { InputTextModule } from 'primeng/inputtext';
import { PanelModule } from 'primeng/panel';
import { ButtonModule } from 'primeng/button';
import { MessagesModule } from 'primeng/messages';
import { RippleModule } from 'primeng/ripple';


//componentes
import { LoginComponent } from './login/login.component';
import { AuthComponent } from './auth.component';


const modulos = [
  PanelModule,
  ButtonModule,
  InputTextModule,
  MessagesModule,
  RippleModule
]

@NgModule({
  declarations: [LoginComponent, AuthComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    AuthRouting,
    modulos
  ],
})
export class AuthModule { }
