import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class OverlayService {

  private _loading: boolean = false;
  loadingStatus = new Subject();


  get loading() {
    return this._loading;
  }

  set loading(value: boolean) {
    this._loading = value;
    this.loadingStatus.next(value);
  }

  iniciarLoading() {
    this.loading = true;
  }
  detenerLoading() {
    this.loading = false;
  }


  constructor() { }
}
