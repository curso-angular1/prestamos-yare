import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { OverlayService } from './overlay.service';

@Component({
  selector: 'app-overlay',
  templateUrl: './overlay.component.html',
})
export class OverlayComponent implements OnInit {

  public blocked: boolean = false;
  public loadingSubscription: Subscription;

  constructor(
    private overlayService: OverlayService
  ) { }

  ngOnInit(): void {

    this.loadingSubscription = this.overlayService.loadingStatus.subscribe((value: boolean) => {
      this.blocked = value;
    })
  }
  ngOnDestroy(): void {
    this.loadingSubscription.unsubscribe();

  }

}
