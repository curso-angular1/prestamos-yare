import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PrimeNGConfig } from 'primeng/api';
import { AuthService } from '../../auth/auth.service';
import { OverlayService } from '../../overlay/overlay.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {

  constructor(
    private primengConfig: PrimeNGConfig,
    private router: Router,
    private AuthService: AuthService,
    private overlay: OverlayService
  ) { }

  ngOnInit(): void {
    this.primengConfig.ripple = true;
  }

  salir() {
    this.AuthService.logout();
    this.router.navigateByUrl("auth/login");
  }


}
