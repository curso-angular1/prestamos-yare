import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CajeraUserComponent } from './cajera-user.component';
import { MenuComponent } from './menu/menu.component';

const routes: Routes = [{
  path: '', component: CajeraUserComponent,
  children: [
    { path: '', component: MenuComponent }
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CajeraUserRoutingModule { }
