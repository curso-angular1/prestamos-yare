import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

//oouting
import { CajeraUserRoutingModule } from './cajera-user.routing';

//components
import { CajeraUserComponent } from './cajera-user.component';
import { MenuComponent } from './menu/menu.component';

// modulos
import { ButtonModule } from 'primeng/button';
import { RippleModule } from 'primeng/ripple';

const modulos = [
  ButtonModule,
  RippleModule
];

@NgModule({
  declarations: [CajeraUserComponent, MenuComponent],
  imports: [
    CommonModule,
    modulos,
    CajeraUserRoutingModule
  ]
})
export class CajeraUserModule { }
