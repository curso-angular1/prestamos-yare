import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from './components/auth/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
})
export class AppComponent {
  title = 'prestamos';

  constructor
    (
      private authService: AuthService,
      private router: Router
    ) { }

  ngOnInit(): void {

    // if (this.authService.estaAutenticado()) {
    //   this.authService.getUser().subscribe(res => {
    //     // console.log(res);
    //   }, error => {

    //     this.authService.logout();
    //     this.router.navigate(['/auth/login']);
    //   });
    // } else {
    //   this.authService.logout();
    //   this.router.navigate(['/auth/login']);
    // }


  }
}
